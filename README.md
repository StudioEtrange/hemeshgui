# HemeshGui
 
 by Amnon Owed ([http://amnonp5.wordpress.com](http://amnonp5.wordpress.com))


### About
HemeshGui :

* [https://code.google.com/p/amnonp5/](https://code.google.com/p/amnonp5/)
* [http://www.creativeapplications.net/processing/hemesh-and-hemeshgui-processing/](http://www.creativeapplications.net/processing/hemesh-and-hemeshgui-processing/)

Sunflow & Processing : 

* [http://amnonp5.wordpress.com/2010/09/28/sunflow-processing/](http://amnonp5.wordpress.com/2010/09/28/sunflow-processing/)


### Changelog

#### v0.3

changes and fixes by StudioEtrange/nomorgan ([http://www.studio-etrange.net](http://www.studio-etrange.net))

New Features

* Sunflow light control (position and color of sunsky light, directional and sphere light)
* Sunflow basic scene (simple background)
* New Sunflow shaders included (diffuse, constant, phong, Glass, mirror, anisotropic ward, wireframe)
* Control over parameters of sunflow shaders 
* New meshes (torus, grid, convexhull)
* UI tweak and help tooltips
* Mouse control (Left-mouse / right mouse)

Support for

* Processing version 2.1.x
* Hemesh version 1.81
* controlP5 version 2.0.4

#### v0.2

* Adapted HemeshGui to run under the latest Processing version (1.5.1)
* Adapted HemeshGui to run under the latest Hemesh version (Beta 1.4.9)
 
### Require

* Processing 2.0 [http://www.processing.org/](http://www.processing.org/)
* Hemesh 1.81 [http://hemesh.wblut.com/](http://hemesh.wblut.com/)
* controlP5 2.0.4 [http://www.sojamo.de/libraries/controlP5](http://www.sojamo.de/libraries/controlP5)

### Librairies Included

These librairies are included in HemeshGui

* SunflowAPIAPI r29 - Dec 11, 2010 - [https://code.google.com/p/sunflowapiapi/](https://code.google.com/p/sunflowapiapi/)
* Sunflow 0.7.3 [http://www.polyquark.com/opensource/](http://www.polyquark.com/opensource/) (original project : http://sunflow.sourceforge.net/ )
 
### Installation

#### Initial Steps

* Step 1 : Install Processing 2.0
* Step 2 : Inside Processing IDE, install Hemesh and controlP5

#### Install dev version from git

* cd PROCESSING_SKETCHBOOK_FOLDER
* git clone https://bitbucket.org/StudioEtrange/hemeshgui.git

#### Install last stable version

* download last version from https://bitbucket.org/StudioEtrange/hemeshgui/downloads
* unzip file into PROCESSING_SKETCHBOOK_FOLDER

### Licenses

* HemeshGui is released under LGPL-2.1 [http://opensource.org/licenses/LGPL-2.1](http://opensource.org/licenses/LGPL-2.1)
* Sunflow Rendering Engine is integrated into HemeshGui under MIT Licence [http://opensource.org/licenses/MIT](http://opensource.org/licenses/MIT)
* SunflowAPIAPI is integrated into HemeshGui under GPL-2.0 [http://opensource.org/licenses/GPL-2.0](http://opensource.org/licenses/GPL-2.0)


