
class Modifier {
  int select;
  float[] values = new float[5];
  int index, currentIndex;
/*
  Modifier (int index, int select, float[] values) {
    this.index = index;
    this.select = select;
    this.values = values;
    currentIndex = index;
    menu();
    
  }*/
  
  Modifier (int index, int select, float[] values, boolean createControlP5) {
    this.index = index;
    this.select = select;
    this.values = values;
    this.currentIndex = index;
    println("Constructor Modifier index="+ index + " select=" + select);    
    if (createControlP5) menu();
  }


  // run the modifier
  void hemesh() {
    hemeshModify(this.select, this.values[0], this.values[1], this.values[2], this.values[3], this.values[4]);
  }

  // display gui elements for the modifier
  void menu() {
    println("BEGIN Menu Modifier create controlsP5");
    controlP5.addButton("modifier" + this.index + "s" + this.select)
            .setPosition(300 + int(this.index/5) * 240, 20 + this.index * 130 - int(this.index/5) * 650)
            .setSize(200, 15)
            .setLabel(numToName(this.select) + "   [remove]")
            ;
    //controlP5.addButton("modifier" + this.index + "s" + select, 0, 300 + int(this.index/5) * 240, 20 + this.index * 130 - int(this.index/5) * 650, 200, 15);
    //((Button)controlP5.controller("modifier"+this.index+s+select)).setLabel(numToName(select) + "   [remove]");
    //myButton.setId(select);
    println("SLIDER 0-3 Menu Modifier create controlsP5");
    for (int i=0; i<4; i++) {
      print(" *i:"+i + " value :" + this.values[i]); 
      controlP5.addSlider("p"+this.index + "v" + i + "MX" )
            .setPosition(300 + int(this.index/5) * 240, 20 + this.index * 130 - int(this.index/5) * 650 + (i+1) * 20)
            .setLabel("")
            .setRange(0.0f,10.0f)
            .setValue(this.values[i])
            .setSize(200,15)
           ;
            
      //controlP5.addSlider("p"+this.index + "v" + i, 0,10, values[i], 300 + int(this.index/5) * 240, 20 + this.index * 130 - int(this.index/5) * 650 + (i+1) * 20, 200, 15);
      //((Slider)controlP5.controller("p"+this.index + "v" + i)).setLabel("");
      
    }
     println("SLIDER 4 Menu Modifier create controlsP5");
    controlP5.addSlider("p"+this.index + "v4MX")
            .setPosition(300 + int(this.index/5) * 240, 20 + this.index * 130 - int(this.index/5) * 650 + (5) * 20)
            .setLabel("")
            .setRange(0,100)
            .setValue(this.values[4])
            .setSize(200,15)
           ;
    //controlP5.addSlider("p"+this.index + "v4", 0,100, values[4], 300 + int(this.index/5) * 240, 20 + this.index * 130 - int(this.index/5) * 650 + (4+1) * 20, 200, 15).setLabel("");
    //((Slider)controlP5.controller("p"+this.index + "v" + 4)).setLabel("");
    println("END Menu Modifier create controlsP5");
  }

  // reposition modifier gui if an earlier modifier is removed (aka everything moves up one place)
  void newMenu() {
    if (this.index != this.currentIndex) {
      controlP5.remove("modifier" + this.currentIndex+ "s" + this.select);
      println("newMenu : remove modifier" + this.currentIndex+ "s" + this.select);
      for (int i=0; i<5; i++) {
        println("newMenu(): remove "+"p"+ this.currentIndex+"v"+i+"MX");
        controlP5.remove("p"+this.currentIndex+"v"+i+"MX");
      }
      this.currentIndex = this.index;
      menu();
    }
  }
}

